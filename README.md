[![](https://images.microbadger.com/badges/image/rodrigozc/jdk8-consul.svg)](https://microbadger.com/images/rodrigozc/jdk8-consul "Get your own image badge on microbadger.com") [![](https://images.microbadger.com/badges/version/rodrigozc/jdk8-consul.svg)](https://microbadger.com/images/rodrigozc/jdk8-consul "Get your own version badge on microbadger.com")

# JDK8 + Consul

## Description

This image starts `Java Spring Boot Application` and `Consul` processes in folder `/docker/run`, there are template configurations for `Consul` and `Java Spring Boot Application`.
