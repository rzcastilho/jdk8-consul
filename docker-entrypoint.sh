#!/bin/sh
set -e

# Configure environment
/config/configure_environment.sh

source /etc/environment

# Start all scripts in /docker/run
for COMMAND in `find /docker/run -maxdepth 1 -name '*.sh' | sort`; do
  $COMMAND
done

DAEMON=tail
ARGS="-F"
FILE="/app/logs/service.log"

while [ ! -f $FILE ]; do
  sleep 5
done

$DAEMON $ARGS $FILE
