#!/bin/sh

set -e

source /etc/environment

# Verify if APPLICATION_NAME exists, if not exists set default value
if [ -z ${APPLICATION_NAME+x} ]; then
	APPLICATION_NAME=$APPLICATION_DEFAULT_NAME
fi
export APPLICATION_NAME

# Verify if CONSUL_SERVER exists, if not exists set default value
if [ -z ${CONSUL_SERVER+x} ]; then
	CONSUL_SERVER=$CONSUL_DEFAULT_SERVER
fi
export CONSUL_SERVER

# Verify if CONSUL_INTERFACE exists, if not exists set default value
if [ -z ${CONSUL_INTERFACE+x} ]; then
	CONSUL_INTERFACE=$CONSUL_DEFAULT_INTERFACE
fi
export CONSUL_INTERFACE

# Verify if CONSUL_DATACENTER exists, if not exists set default value
if [ -z ${CONSUL_DATACENTER+x} ]; then
        CONSUL_DATACENTER=$CONSUL_DEFAULT_DATACENTER
fi
export CONSUL_DATACENTER

# Get IP from interface defined in CONSUL_INTERFACE
IP=`ip -f inet addr | grep inet | grep -m 1 $CONSUL_INTERFACE | awk '{ print $2; }' | awk 'BEGIN { FS="/"; } { print $1; }'`

if [ "$IP" == "" ]; then
	echo "Interface $CONSUL_INTERFACE not found!"
	exit 1
fi

DAEMON=consul
ARGS="agent -advertise $IP -client 0.0.0.0 -datacenter $CONSUL_DATACENTER -retry-join $CONSUL_SERVER -data-dir /var/consul -config-dir /etc/consul -node $APPLICATION_NAME-`hostname` -node-id "`cat /proc/sys/kernel/random/uuid`

# Verify if process is already running
if [ -e /var/run/$DAEMON.pid ]; then
	# Kill process and remove PID file
	PID=`cat /var/run/$DAEMON.pid`
  rm /var/run/$DAEMON.pid
	EXISTS=`ps -ef | awk '{ print $2; }' | grep -E "^$PID" | wc -l`
	if [ ${EXISTS} -ne 0 ]; then
		kill $PID
	fi
fi

# Start process and save PID
nohup $DAEMON $ARGS 1>/var/log/$DAEMON.out 2>/var/log/$DAEMON.err &
echo $! > /var/run/$DAEMON.pid

exit 0
