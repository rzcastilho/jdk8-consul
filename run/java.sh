#!/bin/sh

set -e

source /etc/environment

# Verify if APPLICATION_NAME exists, if not exists set default value
if [ -z ${APPLICATION_NAME+x} ]; then
	APPLICATION_NAME=$APPLICATION_DEFAULT_NAME
fi
export APPLICATION_NAME

DAEMON=java
ARGS="--spring.profiles.active=default $ARGS"
JAVA_OPTS="-Djava.security.egd=file:/dev/./urandom -Duser.timezone=$LOCALTIME -Dfile.encoding=$ENCODING $JAVA_OPTS"

# Verify if process is already running
if [ -e /var/run/$DAEMON.pid ]; then
	# Kill process and remove PID file
	PID=`cat /var/run/$DAEMON.pid`
  rm /var/run/$DAEMON.pid
	EXISTS=`ps -ef | awk '{ print $2; }' | grep -E "^$PID" | wc -l`
	if [ ${EXISTS} -ne 0 ]; then
		kill $PID
	fi
fi

# Start process and save PID
nohup $DAEMON $JAVA_OPTS -jar /app/app.jar $ARGS 1>/var/log/$DAEMON.out 2>/var/log/$DAEMON.err &
echo $! > /var/run/$DAEMON.pid

exit 0
