FROM frolvlad/alpine-oraclejdk8:slim

LABEL author.name="Rodrigo Zampieri Castilho"
LABEL author.email="rodrigo.zampieri@gmail.com"
LABEL repository.url="https://bitbucket.org/rcastilho/jdk8-consul"

ENV CONSUL_VERSION "1.2.0"
ENV CONSUL_TEMPLATE_VERSION "0.19.5"

ENV APPLICATION_DEFAULT_NAME "jdk8-consul"
ENV CONSUL_DEFAULT_SERVER "consul"
ENV CONSUL_DEFAULT_INTERFACE "eth0"
ENV CONSUL_DEFAULT_DATACENTER "dc1"

ENV LOCALE_DEFAULT "en_US.UTF-8"
ENV LOCALTIME_DEFAULT "UTC"

RUN apk update && apk upgrade && apk add unzip wget ca-certificates iproute2 busybox-extras bind-tools iputils procps tzdata && rm -rf /var/cache/apk/*\
    # Consul Installation
    && mkdir -p /etc/consul \
    && mkdir -p /var/consul \
    && wget --no-check-certificate https://releases.hashicorp.com/consul/${CONSUL_VERSION}/consul_${CONSUL_VERSION}_linux_amd64.zip \
    && unzip consul_${CONSUL_VERSION}_linux_amd64.zip \
    && mv consul /usr/sbin/consul \
    && chmod +x /usr/sbin/consul \
    && rm consul_${CONSUL_VERSION}_linux_amd64.zip \
    # Consul Template Installation
    && mkdir -p /etc/consul-template/templates \
    && wget --no-check-certificate https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip \
    && unzip consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip \
    && mv consul-template /usr/sbin/consul-template \
    && chmod +x /usr/sbin/consul-template \
    && rm consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip

WORKDIR /app

ADD app.jar app.jar
ADD application.ctmpl application.ctmpl
ADD application.yaml application.yaml
ADD consul/service.ctmpl /etc/consul/
ADD consul/service.json /etc/consul/
ADD consul-template/ /etc/consul-template/
ADD run/ /docker/run/
ADD config/ /config/
ADD *.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]
