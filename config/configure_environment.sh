#!/bin/sh

if [ -e /etc/environment ]; then
	echo "--------------------------------------------------------------------------------"
	echo "The environment has already been configured:"
	cat /etc/environment
	exit 0
fi

echo "--------------------------------------------------------------------------------"
echo "Configuring environment..."

# Verify if APPLICATION_NAME exists, if not exists set default value
if [ -z ${APPLICATION_NAME+x} ]; then
	APPLICATION_NAME=$APPLICATION_DEFAULT_NAME
fi

# Verify if LOCALE exists, if not exists set default value
if [ -z ${LOCALE+x} ]; then
	LOCALE=$LOCALE_DEFAULT
fi

# Setting LOCALE and ENCODING variables
LANG="$LOCALE"
ENCODING=`echo $LOCALE | awk 'BEGIN { FS="."; } { print tolower($2); }'`
LOCALE="$LANG "`echo $ENCODING | awk '{ print toupper($0); }'`

# Verify if LOCALTIME exists, if not exists set default value
if [ -z ${LOCALTIME+x} ]; then
	LOCALTIME=$LOCALTIME_DEFAULT
fi

# Configuring /etc/environment
cat > /etc/environment <<- EOF
APPLICATION_NAME="$APPLICATION_NAME"
LOCALE="$LOCALE"
LOCALTIME="$LOCALTIME"
ENCODING="$ENCODING"
LANG="$LANG"
export APPLICATION_NAME LOCALE LOCALTIME ENCODING LANG
EOF
source /etc/environment

cat /etc/environment

echo "--------------------------------------------------------------------------------"
echo "Configuring localtime..."
rm /etc/localtime
ln -s /usr/share/zoneinfo/${LOCALTIME} /etc/localtime
echo ${LOCALTIME} > /etc/timezone
echo "Localtime configured to ${LOCALTIME}."
echo "Current Date/Time: "`date`

echo "--------------------------------------------------------------------------------"
